#!/bin/bash
#SBATCH -t 00:15:00
#SBATCH -J arc_xds
#SBATCH -N 1
#SBATCH --exclusive
#SBATCH --mem-per-cpu 1500

# filenames stdout and stderr - customise, include %j
#SBATCH -o r_xds_%j.out
#SBATCH -e r_xds_%j.err

set -o nounset

# load modules
module purge
module add gopresto
module add XDS Durin

TESTS_SUM_FILE=tests_sum.txt
NEGGIA_PLUGIN=$EBROOTDURIN/lib/durin-plugin.so

if [ $# -lt 1 ];
then
    echo "At least one argument required."
    exit 0
fi

export NAME_TEMPLATE=$1

# define current and working directory
# note: use shared directory out of gpfs
WDIR=$TMPDIR/xds
#WDIR=/local/slurmtmp.$SLURM_JOB_ID/xds
CDIR=$(pwd)

# use original forkxds
mkdir $TMPDIR/bin
cp $EBROOTXDS/bin/forkxds.orig $TMPDIR/bin/forkxds
export PATH=$TMPDIR/bin:$PATH

# record host names
mpirun --map-by node -n $SLURM_JOB_NUM_NODES hostname

if [ $? -ne 0 ]; then
    echo `date` `hostname` $SLURM_JOB_ID 'xds ERR' >> $CDIR/$TESTS_SUM_FILE
    exit 1
fi

# create working directory and copy XDS.INP
mkdir -p $WDIR
cp configs/XDS.INP $WDIR
cd $WDIR

# xds gets stuck with MAXIMUM_NUMBER_OF_PROCESSORS=(72/8)
if [ $SLURM_CPUS_ON_NODE -eq 72 ]
then
  MAXIMUM_NUMBER_OF_JOBS=9
else
  MAXIMUM_NUMBER_OF_JOBS=8
fi

# overwrite LIB in XDS.INP
sed -i '/LIB=/c\LIB='"$NEGGIA_PLUGIN" XDS.INP
sed -i '/NAME_TEMPLATE_OF_DATA_FRAMES=/c\NAME_TEMPLATE_OF_DATA_FRAMES='"$NAME_TEMPLATE" XDS.INP
sed -i '/MAXIMUM_NUMBER_OF_JOBS=/c\MAXIMUM_NUMBER_OF_JOBS='"$(($MAXIMUM_NUMBER_OF_JOBS))" XDS.INP
sed -i '/MAXIMUM_NUMBER_OF_PROCESSORS=/c\MAXIMUM_NUMBER_OF_PROCESSORS='"$(($SLURM_CPUS_ON_NODE*$SLURM_JOB_NUM_NODES/$MAXIMUM_NUMBER_OF_JOBS))" XDS.INP

export LOGFILE=$WDIR/output-$SLURM_JOB_ID.log

# run XDS
xds_par | tee $LOGFILE

ret=$(tail -1 $LOGFILE | awk -F' ' '{print $7}')
clktime=`echo $ret | awk '{ print $NF }'`

# (optionaly) copy results to $CDIR

cd $CDIR

if [ -z "$WDIR" ]
then
	echo "Error: WDIR is an empty variable!"
	exit 1
else
	echo "Removing $WDIR"
	rm -rf $WDIR
fi

echo `date` `hostname` $SLURM_JOB_ID 'xds OK wall-clock='$clktime >> $CDIR/$TESTS_SUM_FILE

# Usage:
#  sbatch j_xds.sh '/data/staff/common/xds_test/presto/tau1-tau_2_\?\?\?\?\?\?.h5'
