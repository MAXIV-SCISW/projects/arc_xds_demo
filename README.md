# Very simple ARC XDS demo

```bash
sbatch -p fujitsu j_xds.sh /data/staff/common/xds_test/presto/tau1-tau_2_\?\?\?\?\?\?.h5
```

Note you will need the following files:

```bash
tau1-tau_2_data_000001.h5
tau1-tau_2_data_000002.h5
tau1-tau_2_data_000003.h5
tau1-tau_2_data_000004.h5
tau1-tau_2_data_000005.h5
tau1-tau_2_data_000006.h5
tau1-tau_2_data_000007.h5
tau1-tau_2_data_000008.h5
tau1-tau_2_data_000009.h5
tau1-tau_2_master.h5
```

Credit:
 - Jie Nan (MAX IV LU), Martin Moche (KTH & NSC): XDS job script and XDS configuration
 - DECTRIS AG (Switzerland): tau1 data
